package main

import (
	"flag"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/zongwb/taskqueue"
)

var (
	redisHostPort = flag.String("r", "localhost:6379", "Redis host:post")
	redisPassword = flag.String("p", "", "Redis password")
	redisPoolSize = flag.Int("c", 100, "redis connection pool size")
	threads       = flag.Int("t", 100, "Num of threads")
	keyFile       = flag.String("f", "", "file that contains a list of keys")
	duration      = flag.Int("d", 30, "Duration to run the test in seconds.")
)

const (
	QUEUE_SIZE  = 10000
	MAX_TIMEOUT = time.Second * 1000
)

func main() {
	flag.Parse()

	// read the keys
	keys, err := readKeys(*keyFile)
	if err != nil {
		logger.Sugar().Fatalf("Fail to read keys, err=%v", err)
		return
	}

	r, err := createRedisInstance(*redisHostPort, *redisPassword, *redisPoolSize, *redisPoolSize, 0)
	if err != nil {
		logger.Sugar().Fatalf("Fail to connect to redis %s, err=%v", *redisHostPort, err)
		return
	}
	getter := NewRedisGetter(r)

	// create the thread pool
	if *threads < 1 {
		*threads = 1
	}
	q := taskqueue.New(*threads, QUEUE_SIZE, MAX_TIMEOUT)
	// start the loop
	run(q, getter, keys, *duration)
	printStats()
}

func createRedisInstance(addr string, password string, poolSize int, minIdleConns int, idleTimeout int) (*redis.Client, error) {
	redisOption := redis.Options{
		Addr:         addr,
		Password:     password,
		PoolSize:     poolSize,
		MinIdleConns: minIdleConns,
		IdleTimeout:  time.Duration(idleTimeout),
	}

	client := redis.NewClient(&redisOption)
	_, err := client.Ping().Result()
	if err != nil {
		client.Close()
		return nil, err
	}

	return client, nil
}
