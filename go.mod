module bitbucket/zongwenbo/redis-stresstest

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/prometheus/client_golang v1.5.1
	github.com/prometheus/common v0.9.1
	github.com/zongwb/taskqueue v0.0.0-20180427061203-da7e1a2d00a5
	go.uber.org/zap v1.14.1
)
