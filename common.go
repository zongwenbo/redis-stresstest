package main

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"os"
	"strings"
	"sync/atomic"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
	"github.com/zongwb/taskqueue"
)

var (
	latencySummary          = "Latency_percentile_milliseconds"
	latencySuccessHistogram = "Latency_success_histogram_milliseconds"
	latencyFailureHistogram = "Latency_failure_histogram_milliseconds"

	rpcDurations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:       latencySummary,
			Help:       "RPC latency percentile distributions.",
			Objectives: map[float64]float64{0.25: 0.01, 0.5: 0.01, 0.70: 0.01, 0.8: 0.01, 0.9: 0.01, 0.95: 0.001, 0.98: 0.001, 0.99: 0.001, 0.995: 0.001},
		},
		[]string{"service"},
	)

	rpcSuccessHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    latencySuccessHistogram,
		Help:    "Success latency histogram distributions.",
		Buckets: []float64{10, 25, 50, 100, 250, 500, 750, 1000, 1500, 2000, 3000, 5000},
	})

	rpcFailureHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    latencyFailureHistogram,
		Help:    "Failure latency histogram distributions.",
		Buckets: []float64{10, 25, 50, 100, 250, 500, 750, 1000, 1500, 2000, 3000, 5000},
	})
)

func init() {
	// Register the summary and the histogram with Prometheus's default registry.
	prometheus.MustRegister(rpcDurations)
	prometheus.MustRegister(rpcSuccessHistogram)
	prometheus.MustRegister(rpcFailureHistogram)
}

func readKeys(keyFile string) ([]string, error) {
	f, err := os.Open(keyFile)
	if err != nil {
		return nil, err
	}
	var keys []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		keys = append(keys, scanner.Text())

	}
	err = scanner.Err()
	return keys, err
}

type recordFunc func(success bool, got bool, d time.Duration)

var (
	successCount  int32 = 1 // to avoid divide-by-zero
	nonexistCount int32 = 0
	failureCount  int32 = 0
	active        int32 = 0
	totalTime     int64 = 0
	maxLatency    int64 = 0
	minLatency    int64 = 0
)

func recordOne(success bool, exist bool, d time.Duration) {
	dd := d.Milliseconds()
	if success {
		if exist {
			atomic.AddInt32(&successCount, 1)
			totalTime = totalTime + dd
			if dd > maxLatency {
				maxLatency = dd
			}
			if dd < minLatency {
				minLatency = dd
			}
			rpcDurations.WithLabelValues("Success").Observe(float64(dd))
			rpcSuccessHistogram.Observe(float64(dd))
		} else {
			atomic.AddInt32(&nonexistCount, 1)
			rpcDurations.WithLabelValues("Non-exist").Observe(float64(dd))
			rpcSuccessHistogram.Observe(float64(dd))
		}
	} else {
		atomic.AddInt32(&failureCount, 1)
		rpcDurations.WithLabelValues("Failure").Observe(float64(dd))
		rpcFailureHistogram.Observe(float64(dd))
	}
	logger.Sugar().Infof("active %08d, good %08d, bad %08d, time %08d ms", active, successCount, failureCount, dd)
}

func run(q *taskqueue.TaskQueue, getter Getter, keys []string, duration int) error {
	t := time.NewTimer(time.Second * time.Duration(duration))
	idx := 0
	sz := len(keys)
	start := time.Now()
	count := 0
	for {
		select {
		case <-t.C:
			t.Stop()
			goto final
		default:
			q.Post(&task{
				getter: getter,
				key:    keys[idx],
				record: recordOne,
			}, 0)
			idx++
			idx = idx % sz
			count++
		}
	}
final:
	q.CloseAndWait()
	span := time.Now().Sub(start).Milliseconds()
	logger.Sugar().Infof("\n######## Average throughput (QPS): %.0f\n", float64(count)*1000/float64(span))
	return nil
}

// Getter is an interface so that you can implement load testing i
// again different services (http, grpc, etc.).
type Getter interface {
	Get(key string) ([]byte, error)
}

type RedisGetter struct {
	client *redis.Client
}

func NewRedisGetter(c *redis.Client) Getter {
	return &RedisGetter{
		client: c,
	}
}

func (r *RedisGetter) Get(key string) ([]byte, error) {
	val, err := r.client.Get(key).Result()
	return []byte(val), err
}

// task implements taskqueue.Task interface.
type task struct {
	key    string
	record recordFunc // callback to record result
	getter Getter
}

func (t *task) Do() error {
	atomic.AddInt32(&active, 1)
	start := time.Now()
	_, err := t.getter.Get(t.key)
	if err != redis.Nil && err != nil {
		logger.Sugar().Errorf("err %v", err)
	}
	lapsed := time.Now().Sub(start)
	t.record(err == nil || err == redis.Nil, err == nil, lapsed)
	atomic.AddInt32(&active, -1)
	return err
}

func printStats() {
	var buf bytes.Buffer
	printPrometheusStats(&buf, expfmt.FmtProtoText)
	slogger := logger.Sugar()
	slogger.Infof(string(buf.String()))
	slogger.Infof("---------------------------------\nSummary:\n")
	slogger.Infof("\tsuccessful gets: %d\n", successCount)
	slogger.Infof("\tnon-exist gets: %d\n", nonexistCount)
	slogger.Infof("\tfailed gets: %d\n", failureCount)
	slogger.Infof("\tmax latency: %dms\n", maxLatency)
	slogger.Infof("\tmin latency: %dms\n", minLatency)
	slogger.Infof("\tavg latency: %dms\n", totalTime/int64(successCount))
}

func printPrometheusStats(w io.Writer, contentType expfmt.Format) {
	mfs, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		return
	}

	enc := expfmt.NewEncoder(w, contentType)
	for _, mf := range mfs {
		if strings.HasPrefix(*mf.Name, "go_") {
			continue
		}
		if err := enc.Encode(mf); err != nil {
		}
	}
	if closer, ok := w.(io.Closer); ok {
		closer.Close()
	}
}
